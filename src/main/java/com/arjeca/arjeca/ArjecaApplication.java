package com.arjeca.arjeca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArjecaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArjecaApplication.class, args);
    }

}
